import React from 'react';

const NotFoundPage = () => {
  return <div className="flex justify-center items-center h-screen">Page Not Found</div>;
};

export default NotFoundPage;
